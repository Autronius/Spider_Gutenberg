import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    
    static Pattern txtLink_Pattern = Pattern.compile("(?:(?:https?|ftp|file)://)*[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|].txt"); //((?!-)[A-Za-z0-9-]{1,63}(?<!-)\\.)+[A-Za-z]{2,6}"); //http://www.mkyong.com/regular-expressions/domain-name-regular-expression-example/
    static Pattern filename_Pattern = Pattern.compile("[-a-zA-Z0-9+&@#%=~_|]*.txt");
    
    /*Args[0]: name of file to stick emails into
     * args[1]: root url of sites to crawl
     * args[2]: number of txt documents to find
     */
    public static void main(String[] args){
        
    	String dataDirectory = args[0];
        
        String rootWebsiteHTML = getHTMLFromURL(args[1]);
        Matcher textFileLink_matcher = txtLink_Pattern.matcher(rootWebsiteHTML);
        Hashtable<String, Boolean> visitedHashTable = new Hashtable<String, Boolean>();
        visitedHashTable.put(args[1], true);
        
        //getTxtLinks
        List<String> links = new ArrayList<String>();
        List<String> filenames = new ArrayList<String>();
        findResource(textFileLink_matcher, links, filenames, visitedHashTable);
        
        
        for(int i = 0; (i < Integer.parseInt(args[2])) && (i < links.size()); i++){
            String txt;
        	if(links.get(i).contains("gutenberg")){
        		 txt = getHTMLFromURL(links.get(i));
        	}
        	else{
        		 txt = getHTMLFromURL("http://www.gutenberg.ca/" + links.get(i));	
        	}
           
            
            saveTxtResource(dataDirectory, filenames.get(i), txt); 
            
        }
        
        System.out.println("Total Found: " + links.size());
        System.out.println("S-U-C-C-E-S-S! That is how you spell 'Success'");
        
    }
    
    private static void saveTxtResource(String directory, String filename, String txt) {
		// TODO save txt file to new file in 3 groups. 90% in the main training folder, 5% to a test set, and 5% to a verification set. 
    	//save emails to file
    	Writer output = null;
        try{
            output = new BufferedWriter(new FileWriter((directory + "/test/" + filename), true));
        }catch(Exception e){
            e.printStackTrace();
        }
        
        if(output != null){
            try {
                output.write(txt);
                output.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
	}

	public static void findResource(Matcher patternMatcher, List<String> listToAddTo, List<String> keys, Hashtable<String, Boolean> doNotAddList){
        
        Matcher matcher = patternMatcher;
        List<String> resourcesFound = listToAddTo;
        
        while (matcher.find()) {
        	if(doNotAddList.get(matcher.group()) == null){
        		resourcesFound.add(matcher.group());
        		Matcher filenameMatcher = filename_Pattern.matcher(matcher.group());
        		if(filenameMatcher.find())
        		{
        			keys.add(filenameMatcher.group());
        			System.out.println("Filename: " + filenameMatcher.group());
        		}
        		//ensure we don't add it again if we've already found it
        		doNotAddList.put(matcher.group(), true);
        		System.out.println("Found: " + matcher.group());
        	}
        }
    }

    public static String getHTMLFromURL(String Url){
        
        URL url;
        String html = "";
         
        try {
            // get URL content

            url = new URL(Url);
            URLConnection conn = url.openConnection();

            // open the stream and put it into BufferedReader
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
           
            String inputLine;
            while ((inputLine = br.readLine()) != null) {
                html += inputLine;
            }
            br.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
	        
        return html;
    }
}
